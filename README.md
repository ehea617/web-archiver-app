# Web Archiver Chrome Extension
Google Chrome extension that adds a button to the browser allowing you to save a webpage to the Internet Archive.

Chrome Web Store: [https://chrome.google.com/webstore/detail/web-archiver/gjpgpobcdndcdcidmgcmlphbomllapjp](https://chrome.google.com/webstore/detail/web-archiver/gjpgpobcdndcdcidmgcmlphbomllapjp)
